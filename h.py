import flask,json,re
#建立一个server服务
server=flask.Flask(__name__)
#使用装饰器方法 添加接口地址和定义请求方式
@server.route('/api/user/login',methods=['post'])
#定义一个登录方法
def user_login():
    # 将请求参数转为json串
    data=flask.request.json
    #判断是否接收到json串格式的数据
    if data:
        #获取用户名
        username=data.get('username')
        #获取密码
        password=data.get('password')
        #获取验证码
        code=data.get('code')
        #获取记得我，默认正确
        rememberme=data.get('rememberme','true')
        #匹配用户名校验正则表达式
        path_name=re.compile(r"[\u4e00-\u9fa5]")
        #匹配密码校验规则正则表达式
        path_pass=re.compile(r"^(?=.*[a-zA-Z])(?=.*[0-9])[A-Za-z0-9]{8,18}$")
        #判断用户名,密码,验证码,记住我是否为必填参数
        if username and password and code and rememberme:
            #校验用户名不匹配校验规则
           if not path_name.match(username):
               #返回提示
               res = {"error_code": 3000, "msg": "用户名不合法！"}
            #校验密码不匹配校验规则
           elif not path_pass.match(password):
               #返回提示
               res = {"error_code": 3001, "msg": "密码格式不正确"}
           #验证码校验规则
           elif len(code)!=4 or not code.isdigit():
               #返回提示
               res={"error_code": 3002, "msg": "验证码格式不正确"}
           else:
               #未使用orm框架映射数据库，并且也未安装数据库登录数据暂无 ，手机号和密码是否在数据库校验暂时写死。
               result="李兆鹏"
               result2="lzp336645"
               #判断用户名或密码不正确
               if result!=username or result2!=password:
                   #返回提示
                   res = {"error_code": 1000, "msg": "用户名或密码错误"}
               else:
                   res =  {
    "msg": "登录成功!",
    "code": 200,
    "data": {
        "tooken": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1NzI1Nzc2ODYsImlhdCI6MTU3MjU3NDA4NiwidXNlcm5hbWUiOiJsYzEyMzQ1NiJ9.7F1dGeXjsLFsLR1ZIvwkMPzdJp2HL-sx8VehK59D_l4",
        "addTime": 1572320068000,
        "skinColor": "null",
        "registerTime": "null",
        "roleId": 5,
        "addUser": 45,
        "ip": "100.125.58.95",
        "loginTimes": 20,
        "updateUser": 52,
        "remark": "",
        "updateTime": 1572328152000,
        "userName": "李兆鹏",
        "lastLoginTime": 1572574087051,
        "userRoles": "null",
        "mobilePhone": "",
        "roleName": "李兆鹏",
        "id": 48,
        "state": 0,
        "projectId": "null",
        "account": "lc123456",
        "loginSource": "null",
        "status": 0
    },
    "executeTime": 487
}
        else:
            res = {"error_code": 3004, "msg": "请填写必填参数!"}
    else:
        res = {"error_code": 3005, "msg": "入参必须是json类型"}
    return  json.dumps(res)





if __name__ == '__main__':
   server.run(port=5001)













